# fc-fontname

print font family name of a font file

usage:

```sh
$ fc-fontname "FontFile"
```

example:

```sh
$ fc-fontname ~/.local/share/fonts/Hack-Regular.ttf
# Hack
```

FontPath is the font file

installation:

```sh
$ git clone https://gitlab.com/Lyfv/fc-fontname.git
$ cd fc-fontname
$ chmod +x fc-fontname # or with sudo
```

or you can add it into bin directory

```sh
$ ln -s ~PathToFcFontName/fc-fontname /usr/bin/ # or with sudo
```
